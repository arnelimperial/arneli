import { Link } from "preact-router/match";

const NotFound = () => (
  <main>
    <article>
      <section>
        <h3 class="h3">You found a broken link.</h3>
        <p>
          Maybe you'd be better off at the{" "}
          <Link href="/" class="anchor">
            home
          </Link>{" "}
          page.
        </p>
      </section>
    </article>
  </main>
);

export default NotFound;
