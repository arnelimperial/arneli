const RandomStr = (len, arr) => {
  let ans = "";
  for (let i = len; i > 0; i--) {
    ans += arr[Math.floor(Math.random() * arr.length)];
  }
  return ans;
};

const Roll = () => {
  let roll = RandomStr(6, "1234589ARNELZymTUgX#%_0o");
  return roll;
};

const RandomVal = () => {
  let val = Roll();
  return val;
};

export default RandomVal;
