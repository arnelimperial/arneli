import { Component } from "preact";
import RandomVal from "./randomVal";
import axios from "axios";
import style from "./style.css";
// import Loadable from "react-loadable";
// import Spinner from "../../components/spinner";

const validEmailRegex = RegExp(
  /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
);

const validateForm = (errors) => {
  let valid = true;
  Object.values(errors).forEach((val) => val.length > 0 && (valid = false));
  return valid;
};

class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: "",
      last_name: "",
      subject: "",
      email: "",
      message: "",
      security: "",
      code: RandomVal(),
      errors: {
        first_name: "",
        last_name: "",
        subject: "",
        email: "",
        message: "",
        security: "",
      },
    };
  }

  handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;
    let errors = this.state.errors;
    const curr_code = this.state.code;

    switch (name) {
      case "first_name":
        errors.first_name =
          value.length < 2
            ? "First Name must be at least 2 characters long!"
            : "";
        break;
      case "last_name":
        errors.last_name =
          value.length < 2
            ? "Last Name must be at least 2 characters long!"
            : "";
        break;
      case "subject":
        errors.subject =
          value.length < 4 ? "Subject must be at least 4 characters long!" : "";
        break;
      case "email":
        errors.email = validEmailRegex.test(value) ? "" : "Email is not valid!";
        break;
      case "message":
        errors.message =
          value.length < 4 ? "Message must be at least 4 characters long!" : "";
        break;
      case "security":
        errors.security = value !== curr_code ? "You return a wrong code!" : "";
        break;
      default:
        break;
    }

    this.setState({ errors, [name]: value });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    if (validateForm(this.state.errors)) {
      console.log("Valid Form");
    } else {
      console.log("Invalid Form");
    }
    const data = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      subject: this.state.subject,
      email: this.state.email,
      message: this.state.message,
    };
    const fname = data.first_name;
    const lname = data.last_name;

    axios
      .post(process.env.PREACT_APP_ENDPOINT_MESSAGES, data)
      .then((response) => {
        let successmsg = document.querySelector("#formMsg");
        successmsg.innerHTML = `Thank's ${fname} ${lname} your message has been submitted.`;
      })
      .catch((error) => {
        let successmsg = document.querySelector("#formMsg");
        successmsg.innerHTML = `Sorry ${fname}, your form did not submit for some reason. Please try again.`;
      });

    this.setState({
      first_name: "",
      last_name: "",
      subject: "",
      email: "",
      message: "",
      security: "",
    });
  };

  render() {
    const { errors, isLoading } = this.state;
    return (
      <>

        <main>
          <section>
            <h3 class="h3">Contact</h3>
            <p>The most sweetest things you can do</p>
            <form method="POST" onSubmit={this.handleSubmit}>
              {/* <CSRFToken /> */}
              <fieldset class={style.contact__fieldset}>
                <div class={style.first__name}>
                  <label htmlFor="first-name-field">First Name</label>
                  <input
                    type="text"
                    value={this.state.first_name}
                    name="first_name"
                    placeholder="First name"
                    class={style.contact__field}
                    onChange={this.handleChange}
                    noValidate
                  />
                  {errors.first_name.length > 0 && (
                    <span class={style.error__msg}>{errors.first_name}</span>
                  )}
                </div>

                <div>
                  <label htmlFor="last-name-field">Last Name</label>
                  <input
                    type="text"
                    value={this.state.last_name}
                    name="last_name"
                    placeholder="Last name"
                    class={style.contact__field}
                    onChange={this.handleChange}
                    noValidate
                  />
                  {errors.last_name.length > 0 && (
                    <span class={style.error__msg}>{errors.last_name}</span>
                  )}
                </div>
                <div>
                  <label htmlFor="subject-field">Subject</label>
                  <input
                    type="text"
                    value={this.state.subject}
                    name="subject"
                    placeholder="Your concern or intent."
                    class={style.contact__field}
                    onChange={this.handleChange}
                    noValidate
                  />
                  {errors.subject.length > 0 && (
                    <span class={style.error__msg}>{errors.subject}</span>
                  )}
                </div>
                <div>
                  <label htmlFor="email-field">Email</label>
                  <input
                    type="email"
                    value={this.state.email}
                    name="email"
                    placeholder="A valid email"
                    class={style.contact__field}
                    onChange={this.handleChange}
                    noValidate
                  />
                  {errors.email.length > 0 && (
                    <span class={style.error__msg}>{errors.email}</span>
                  )}
                </div>
                <div>
                  <label htmlFor="message-field">Message</label>
                  <textarea
                    value={this.state.message}
                    name="message"
                    onChange={this.handleChange}
                    class={style.contact__field}
                    placeholder="Message here..."
                    id=""
                    noValidate
                  ></textarea>
                  {errors.message.length > 0 && (
                    <span class={style.error__msg}>{errors.message}</span>
                  )}
                </div>
                <div>
                  <label htmlFor="security-field">Security Question</label>
                  <p class="spaces">
                    Code:{" "}
                    <span class={style.security__code}>{this.state.code}</span>
                  </p>
                  <input
                    type="text"
                    value={this.state.security}
                    name="security"
                    placeholder="Case-sensitive. Type the code correctly."
                    class={style.contact__field}
                    onChange={this.handleChange}
                    noValidate
                  />
                  {errors.security.length > 0 && (
                    <span class={style.error__msg}>{errors.security}</span>
                  )}
                </div>
                <div class={style.contact__button}>
                  <button type="submit" class={style.btn}>
                    SUBMIT
                  </button>
                </div>
                <div class={style.success__msg} id="formMsg">
                  <label htmlFor=""></label>
                </div>
              </fieldset>
            </form>
          </section>
        </main>
      </>
    );
  }
}

export default Contact;
