import { Component } from "preact";
import axios from "axios";
import Markdown from "preact-markdown";
import style from "./style.css";

class SinglePost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      post: {
        content: "",
      },
    };
  }
  componentDidMount() {
    const params = this.props;
    let endpoint = process.env.PREACT_APP_ENDPOINT_BLOGS;
    axios.get(endpoint.concat(params.slug)).then((res) => {
      const post = res.data;
      this.setState({ post });
    });
  }
  render() {
    const { post } = this.state;
    return (
      <div>
        <main>
          <article>
            <section>
              <div class={style.singlepost__main__div}>
                <h3 class="h3 spaces" key={post.id}>
                  {post.title}
                </h3>
                <small>
                  <strong>Published: {post.created}</strong>
                </small>
                <br />
                <small>
                  <strong>Updated: {post.updated}</strong>
                </small>
                {post.photo && (
                  <p>
                    <img src={post.photo} alt="Blog photo" />
                  </p>
                )}
                <br />
                <br />
                <Markdown markdown={post.content} />
              </div>
            </section>
          </article>
        </main>
      </div>
    );
  }
}

export default SinglePost;
