import style from './style.css';
const AI = () => (
  <p>
    {/* <a
      href="https://drive.google.com/file/d/16pCW1epPtoG6_Pc9J79z5MlBcu49VJd1/view?usp=sharing"
      target="_blank"
      rel="noopener noreferrer"
    > */}
      <img
        src="https://lh3.googleusercontent.com/7cSaYOi8JGnSUFOf_tWTek1mPHx_c8psxqs5e8REsmn8CYoGTbr369aKdw_QcPgvdiFZuIhnjaevYyZy6DP6-efVHqVvBZ3bu45mC3QDovHiRePuYjNMN4bEWaYvjYyieSIsKVdBSA=w2400"
        alt="Arnel Imperial"
        class={style.solo__picture}
      />
    {/* </a> */}
  </p>
);

export default AI;
