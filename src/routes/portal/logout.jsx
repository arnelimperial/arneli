import { h, Component } from "preact";
import axios from "axios";
import { decode } from "js-base64";
import Redirect from "../../components/redirect";

export default class Logout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogout: false,
    };
  }

  handleClick = (e) => {
    e.preventDefault();
    const token = sessionStorage.getItem("token");
    const access_token = decode(token);
    const endpoint = process.env.PREACT_APP_ENDPOINT_AUTH_LOGOUT;
    axios
      .post(endpoint, {
        headers: { Authorization: `Token ${access_token}` },
      })
      .then((res) => {
        sessionStorage.clear();
        //window.location.reload();
        this.setState({ isLogout: true });
        const status = res.status;
        if (status == 200) {
          window.location = "/";
          // <Redirect to="/" />
        }
      })
      .catch((err) => console.log(err));
  };

  render() {
    const { isLogout } = this.state;
    return (
      <main>
        <article>
          <section>
            <h3 className="h3">Logout</h3>
            <p>Are you sure you want to sign out?</p>
            <form method="POST">
              <button onClick={this.handleClick} type="submit">
                Submit
              </button>
            </form>
            {/* {isLogout && <Redirect to={"/"} />} */}
          </section>
        </article>
      </main>
    );
  }
}
