import { h, Component } from "preact";
import axios from "axios";
import style from "../contact/style.css";
//import csrftoken from "./csrf";
import Redirect from "../../components/redirect";
import { encode } from "js-base64";

class Portal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      isAuthed: false,
      errors: {
        username: "",
        password: "",
      },
    };
  }

  handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;
    let errors = this.state.errors;

    switch (name) {
      case "username":
        errors.username =
          value.length < 1 ? "login credential is required." : "";
        break;
      case "password":
        errors.password = value.length < 1 ? "password is required" : "";
        break;
      default:
        break;
    }

    this.setState({ errors, [name]: value });
  };

  handleSubmit = (event) => {
    event.preventDefault();

    const data = {
      username: this.state.username,
      password: this.state.password,
    };

    const loginName = data.username;

    const axiosConfig = {
      headers: {
        "Content-Type": "application/json",
      },
      auth: {
        username: data.username,
        password: data.password,
      },
    };

    axios
      .post(process.env.PREACT_APP_ENDPOINT_AUTH_LOGIN, data, axiosConfig)
      .then((response) => {
        //let successmsg = document.querySelector("#formMsg");
        //successmsg.innerHTML = response.data.key;
        //console.log(response);
        //Set localStorage
        const token = encode(response.data.key);
        sessionStorage.setItem("token", token);
        this.setState({ isAuthed: true });
        const status = response.status;
        if (status == 200) {
          window.location = "/";
          // <Redirect to="/" />
        }
      })
      .catch((error) => {
        let successmsg = document.querySelector("#formMsg");
        successmsg.innerHTML = `Sorry ${loginName}, there was an error!`;
        //this.setState({ isAuthed: false });
      });
    this.setState({
      username: "",
      password: "",
    });
  };

  render() {
    const { errors, isAuthed } = this.state;
    const uname = this.state.username;
    return (
      <>
        <main>
          <section>
            <h3 class="h3">Portal</h3>
            <form
              method="POST"
              onSubmit={this.handleSubmit}
              enctype="multipart/form-data"
            >
              <fieldset class={style.contact__fieldset}>
                <div class={style.first__name}>
                  <label htmlFor="username-field">Login</label>
                  <input
                    type="text"
                    value={this.state.username}
                    name="username"
                    placeholder="Login credential"
                    class={style.contact__field}
                    onChange={this.handleChange}
                    noValidate
                  />
                  {errors.username.length > 0 && (
                    <span class={style.error__msg}>{errors.username}</span>
                  )}
                </div>

                <div>
                  <label htmlFor="password-field">Password</label>
                  <input
                    type="password"
                    value={this.state.password}
                    name="password"
                    placeholder="Password"
                    class={style.contact__field}
                    onChange={this.handleChange}
                    noValidate
                  />
                  {errors.password.length > 0 && (
                    <span class={style.error__msg}>{errors.password}</span>
                  )}
                </div>

                <div class={style.contact__button}>
                  <button type="submit" name="submit" class={style.btn}>
                    Sign In
                  </button>
                </div>
                {/* {isAuthed && <Redirect to={"/"} />} */}
                <div class={style.success__msg} id="formMsg">
                  <label htmlFor=""></label>
                </div>
              </fieldset>
            </form>
          </section>
          <section></section>
        </main>
      </>
    );
  }
}

export default Portal;
