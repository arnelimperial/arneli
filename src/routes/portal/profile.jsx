import { h, render, Component } from "preact";
import axios from "axios";
import UserData from "./user-data.jsx";
import { decode } from 'js-base64';


export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      email: "",
      name: "",
      id: "",
      profile: [],
    };
  }
  getUser = () => {
    const token = sessionStorage.getItem("token");
    const access_token = decode(token);
    const endpoint = process.env.PREACT_APP_ENDPOINT_AUTH_PROFILE;
    axios
      .get(endpoint, {
        headers: { Authorization: `Token ${access_token}` },
      })
      .then((res) => this.setState({ profile: [...res.data] }))
      .catch((err) => console.log(err));
  };

  componentDidMount() {
    this.getUser();
  }

  userData = () => {
    return this.state.profile.map((data, i) => {
      return <UserData obj={data} key={i} />;
    });
  };

  render() {
    return (
      <main>
        <article>
          <section>
            {this.userData()}
          </section>
        </article>
      </main>
    );
  }
}
