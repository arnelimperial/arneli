import { h, Component } from "preact";
//import { Link } from "preact-router/match";
class UserData extends Component {
  render() {
    return (
      <div>
        <p style={{visibility:"hidden"}}>{this.props.obj.id}</p>
        <h4>{this.props.obj.username}</h4>
        <small>
          <strong>{this.props.obj.email}</strong>
        </small>
        <br />
        {this.props.obj.name && <p>{this.props.obj.name}</p>}
       
      </div>
    );
  }
}

export default UserData;
