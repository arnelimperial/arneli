import { h, Component } from "preact";
import { Link } from "preact-router/match";
//import LazyRoute from "preact-lazy-route";
//import Spinner from "../../components/spinner/small";
import axios from "axios";
import style from "./style.css";
//import Loadable from "preact-loadable";
import PostData from "./post-data";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
    };
  }

  componentDidMount() {
    axios
    .get(process.env.PREACT_APP_ENDPOINT_BLOGS)
    .then((res) => {
      this.setState({ posts: [...res.data] });
    })
    .catch((err) => console.log(err));
  }

  postData() {
    return this.state.posts.map((post, i) => {
      return <PostData obj={post} key={i} />;
    });
  }

  render() {
    return (
      <>
        <div class={style.main__home__div}>
          <h2 class={style.home__h2}>Latest</h2>
          <p>
            A compilation of insights and trials that kept me involved to a
            certain degree. I mark it out so that I don't regret that I forget.
          </p>
          <div>{this.postData()}</div>
        </div>
      </>
    );
  }
}

export default Home;

// {this.state.posts.map((post) => (
//   <div>
//     <div>
//       <h3 key={post.id} class={style.home__h3}>
//         <Link href={`/post/${post.slug}`} class="anchor spaces">
//           {post.title}
//         </Link>
//       </h3>
//       <small>
//         <strong>{post.updated}</strong>
//       </small>
//       <br />
//       {post.description && <p>{post.description}</p>}

//       <hr />
//     </div>
//   </div>
// ))}
