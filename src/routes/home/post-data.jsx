import { h, Component } from "preact";
import { Link } from "preact-router/match";
import style from "./style.css";

class PostData extends Component {
  render() {
    return (
      <>
        <h3 class={style.home__h3}>
          <p id={style.home__post__id}>{this.props.obj.id}</p>
          <Link href={`/post/${this.props.obj.slug}`} class="anchor spaces">
            {this.props.obj.title}
          </Link>
        </h3>
        <small>
          <strong>{this.props.obj.updated}</strong>
        </small>
        <br />
        {this.props.obj.description && <p>{this.props.obj.description}</p>}

        <hr />
      </>
    );
  }
}

export default PostData;
