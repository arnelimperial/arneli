import { h, render, Component } from "preact";
import { Router, Route } from "preact-router";
import Header from "./header";
// Code-splitting is automated for `routes` directory
import Home from "../routes/home";
import About from "../routes/about";
import Resume from "../routes/resume";
import Uses from "../routes/uses";
import SinglePost from "../routes/singlepost";
import Contact from "../routes/contact";
import NotFound from "../routes/notfound";
import Portal from "../routes/portal/index.js";
import Profile from "../routes/portal/profile.jsx";
import Logout from "../routes/portal/logout.jsx";
import Redirect from "./redirect";
import "./app.css";

//import Spinner from "./spinner";

const App = () => (
  <div id="app" class="holder">
    <Header />
    <Router>
      <Home path="/" />
      <About path="/about" />
      <Resume path="/resume" />
      <Uses path="/uses" />
      <Contact path="/contact" />
      <SinglePost path="/post/:slug" />
      <Portal path="/portal" />
      <Profile path="/profile" />
      <Logout path="/logout" />
      <NotFound default />
    </Router>
    <footer>
      <small>
        2019 - {new Date().getFullYear()} by <b>Arnel Imperial</b>
      </small>
    </footer>
  </div>
);

export default App;
