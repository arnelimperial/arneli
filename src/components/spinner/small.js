import style from './style.css';
const Spinner = () => (
  <div class={style.spinner__div__small}>
    <div class={style.loader}></div>
  </div>
);

export default Spinner;