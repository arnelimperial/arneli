import style from './style.css';
const Spinner = () => (
  <div class={style.spinner__div}>
    <h3>Loading...</h3>
    <div class={style.loader}></div>
  </div>
);

export default Spinner;
