import { h, Component } from "preact";
import { Link } from "preact-router/match";
//import { Router, route } from "preact-router";
import HeaderComponent from "./header-component";
import { decode } from "js-base64";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAuthed: false,
    };
  }

  isAuthenticated() {
    if (typeof sessionStorage !== "undefined" && sessionStorage.length > 0) {
      this.setState({ isAuthed: true });
    } else {
      this.setState({ isAuthed: false });
    }
  }

  componentDidMount() {
    this.isAuthenticated();
  }

  render() {
    const isAuthed = this.state.isAuthed;
    let li;
    if (isAuthed) {
      li = (
        <>
          <li>
            <Link href="/profile" activeClassName="active">
              Profile
            </Link>
          </li>
          <li>
            <Link href="/logout" activeClassName="active">
              Logout
            </Link>
          </li>
        </>
      );
    } else {
      li = (
        <li>
          <Link href="/portal" activeClassName="active">
            Portal
          </Link>
        </li>
      );
    }

    return (
      <header class="mt-2">
        <h1 id="coder">
          <Link href="/">Arnel Imperial</Link>
        </h1>
        <nav class="header--nav">
          <ul class="header--ul">
            <HeaderComponent isAuthed={isAuthed} />
            {li}
          </ul>
        </nav>
      </header>
    );
  }
}

export default Header;
