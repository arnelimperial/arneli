import { Link } from "preact-router/match";

const HeaderComponent = () => (
  <>
    <li>
      <Link class="link" activeClassName="active" href="/">
        Home
      </Link>
    </li>
    <li>
      <Link href="/about" activeClassName="active">
        About
      </Link>
    </li>
    <li>
      <Link href="#" activeClassName="active">
        Projects
      </Link>
    </li>
    <li>
      <Link href="/contact" activeClassName="active">
        Contact
      </Link>
    </li>
  </>
);

export default HeaderComponent;
