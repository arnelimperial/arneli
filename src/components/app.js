import { render, Component } from "preact";
import { Router, route } from "preact-router";
import Header from "./header";
// Code-splitting is automated for `routes` directory
import Home from "../routes/home";
import About from "../routes/about";
import Resume from "../routes/resume";
import Uses from "../routes/uses";
import SinglePost from "../routes/singlepost";
import Contact from "../routes/contact";
import NotFound from "../routes/notfound";
import Portal from "../routes/portal/index.js";
import Profile from "../routes/portal/profile.jsx";
import Logout from "../routes/portal/logout.jsx";
import Redirect from "./redirect";
import "./app.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAuthed: false,
    };
  }
  isAuthenticated() {
    if (typeof sessionStorage !== "undefined" && sessionStorage.length > 0) {
      this.setState({ isAuthed: true });
    } else {
      this.setState({ isAuthed: false });
    }
  }

  handleRoute = async (e) => {
    let isAuthed = await this.isAuthenticated();
    switch (e.url) {
      case "/profile":
        if (!isAuthed && sessionStorage.getItem("token") === null)
          route("/portal", true);

        break;
      case "/logout":
        if (!isAuthed && sessionStorage.getItem("token") === null)
          route("/portal", true);

        break;
    }
  };

  render() {
    return (
      <div id="app" class="holder">
        <Header />
        <Router onChange={this.handleRoute}>
          <Home path="/" />
          <About path="/about" />
          <Resume path="/resume" />
          <Uses path="/uses" />
          <Contact path="/contact" />
          <SinglePost path="/post/:slug" />
          <Portal path="/portal" />
          <Profile path="/profile" />
          <Logout path="/logout" />
          <NotFound default />
        </Router>
        <footer>
          <small>
            2019 - {new Date().getFullYear()} by <b>Arnel Imperial</b>
          </small>
        </footer>
      </div>
    );
  }
}

export default App;
