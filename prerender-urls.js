var slugify = require("slugify");

// General
const home = "https://www.arnelimperial.com/";
const author = "Arnel Imperial";
//const desc = "Combining the right tools to the web to be functional is like adding the right ingredients to the cuisine to reanimate someone's organoleptic dreams.";
const desc = "Re-learning web development by harnessing the information from the internet and out to build things that matter to me and maybe to someone's organoleptic dream.";
const imgNormalFB = "https://od.lk/s/ODhfOTA3Nzc0OF8/social-image3.jpg";
const imgNormalTwitter = "https://od.lk/s/ODhfOTA3NjE3NF8/social-image3.png";
const creator = "@ArnelGImperial";
const alt = "Generic image created for the article.";
// Blog
const blogTitle = {
  first: "Set Up a Python 3 Version in a Project Directory on Mac",
  second: "Project Environment Set Up in Python",
  third: "Open Graph Protocol and Twitter Cards in Preact CLI",
  fourth: "Solution for Access Control Allow Origin Issues on Production for Django REST API",
  fifth: "Quarantine Procedure",
  sixth: "Session Storage",

};
const blogSlug = {
  first: slugify(blogTitle.first, { lower: true }),
  second: slugify(blogTitle.second, { lower: true }),
  third: slugify(blogTitle.third, { lower: true }),
  fourth: slugify(blogTitle.fourth, { lower: true }),
  fifth: slugify(blogTitle.fifth, { lower: true }),
  sixth: slugify(blogTitle.sixth, { lower: true }),


};

module.exports = [
  {
    url: "/",
    title: author,
    canonical: home,
    descContent: desc,
    ogType: "website",
    ogUrl: home,
    ogTitle: author,
    ogDesc: desc,
    twitterCard: "summary_large_image",
    twitterUrl: home,
    twitterTitle: author,
    twitterDesc: desc,
    imgFB: imgNormalFB,
    imgTwitter: imgNormalFB,
    twitterCreator: creator,
    imgAlt: "Arnel Imperial's website icon."
  },
  {
    url: "/about",
    title: author.concat(" - About Page"),
    canonical: home.concat("about"),
    descContent: desc.concat(" - About Page"),
    ogType: "article",
    ogUrl: home.concat("about"),
    ogTitle: author.concat(" - About Page"),
    ogDesc: desc.concat(" - About Page"),
    twitterCard: "summary",
    twitterUrl: home.concat("about"),
    twitterTitle: author.concat(" - About Page"),
    twitterDesc: desc.concat(" - About Page"),
    imgFB: imgNormalFB,
    imgTwitter: imgNormalTwitter,
    twitterCreator: creator,
    imgAlt: "Arnel Imperial's website icon."
  },
  {
    url: `/post/${blogSlug.first}`,
    title: blogTitle.first,
    canonical: home.concat(`post/${blogSlug.first}`),
    descContent: "This is how I set up Python 3 environment on Mac.",
    ogType: "article",
    ogUrl: home.concat(`post/${blogSlug.first}`),
    ogTitle: blogTitle.first,
    ogDesc: "This is how I set up Python 3 environment on Mac.",
    twitterCard: "summary_large_image",
    twitterUrl: home.concat(`post/${blogSlug.first}`),
    twitterTitle: blogTitle.first,
    twitterDesc: "This is how I set up Python 3 environment on Mac.",
    imgFB: imgNormalFB,
    imgTwitter: imgNormalFB,
    twitterCreator: creator,
    imgAlt: alt
  },
  {
    url: `/post/${blogSlug.second}`,
    title: blogTitle.second,
    canonical: home.concat(`post/${blogSlug.second}`),
    descContent: "How to create a self-contained project in Python.",
    ogType: "article",
    ogUrl: home.concat(`post/${blogSlug.second}`),
    ogTitle: blogTitle.second,
    ogDesc: "How to create a self-contained project in Python.",
    twitterCard: "summary_large_image",
    twitterUrl: home.concat(`post/${blogSlug.second}`),
    twitterTitle: blogTitle.second,
    twitterDesc: "How to create a self-contained project in Python.",
    imgFB: imgNormalFB,
    imgTwitter: imgNormalTwitter,
    twitterCreator: creator,
    imgAlt: alt
  },
  {
    url: `/post/${blogSlug.third}`,
    title: blogTitle.third,
    canonical: home.concat(`post/${blogSlug.third}`),
    descContent: "Initial configuration of Open Graph and Twitter in Preact CLI (default template)",
    ogType: "article",
    ogUrl: home.concat(`post/${blogSlug.third}`),
    ogTitle: blogTitle.third,
    ogDesc: "Initial configuration of Open Graph and Twitter in Preact CLI (default template)",
    twitterCard: "summary_large_image",
    twitterUrl: home.concat(`post/${blogSlug.third}`),
    twitterTitle: blogTitle.third,
    twitterDesc: "Initial configuration of Open Graph and Twitter in Preact CLI (default template)",
    imgFB: "https://cdn.shopify.com/s/files/1/0021/7710/6033/articles/Dia-de-los-muertos_banner.jpg?v=1547766829",
    imgTwitter: "https://cdn.shopify.com/s/files/1/0021/7710/6033/articles/Dia-de-los-muertos_banner.jpg?v=1547766829",
    twitterCreator: creator,
    imgAlt: alt
  },
  {
    url: `/post/${blogSlug.fourth}`,
    title: blogTitle.fourth,
    canonical: home.concat(`post/${blogSlug.fourth}`),
    descContent: "One of the to ways to resolve the issues in Access Control Allow Origin in Django REST API for JAMstack application.",
    ogType: "article",
    ogUrl: home.concat(`post/${blogSlug.fourth}`),
    ogTitle: blogTitle.fourth,
    ogDesc: "One of the to ways to resolve the issues in Access Control Allow Origin in Django REST API for JAMstack application.",
    twitterCard: "summary_large_image",
    twitterUrl: home.concat(`post/${blogSlug.fourth}`),
    twitterTitle: blogTitle.fourth,
    twitterDesc: "One of the to ways to resolve the issues in Access Control Allow Origin in Django REST API for JAMstack application.",
    imgFB: "https://od.lk/s/ODhfOTA3ODMwOF8/Quito-Ecuador-4.jpg",
    imgTwitter: "https://od.lk/s/ODhfOTA3ODMwOF8/Quito-Ecuador-4.jpg",
    twitterCreator: creator,
    imgAlt: alt
  },
  {
    url: `/post/${blogSlug.fifth}`,
    title: blogTitle.fifth,
    canonical: home.concat(`post/${blogSlug.fifth}`),
    descContent: "Opinion on NodeJs project environment separation procedure by using Python's Nodeenv.",
    ogType: "article",
    ogUrl: home.concat(`post/${blogSlug.fifth}`),
    ogTitle: blogTitle.fifth,
    ogDesc: "Opinion on NodeJs project environment separation procedure by using Python's Nodeenv.",
    twitterCard: "summary_large_image",
    twitterUrl: home.concat(`post/${blogSlug.fifth}`),
    twitterTitle: blogTitle.fifth,
    twitterDesc: "Opinion on NodeJs project environment separation procedure by using Python's Nodeenv.",
    imgFB: "https://od.lk/s/ODhfOTA4MTA3MV8/received_642731703012944.jpg",
    imgTwitter: "https://od.lk/s/ODhfOTA3OTQyOF8/nanays-plant.jpg",
    twitterCreator: creator,
    imgAlt: "A beautiful plant taken from backyard garden by Dulzura Gonzales Imperial."
  },

  {
    url: `/post/${blogSlug.sixth}`,
    title: blogTitle.sixth,
    canonical: home.concat(`post/${blogSlug.sixth}`),
    descContent: "Session storage snippets for login authentication in Preact-CLI and Django DRF.",
    ogType: "article",
    ogUrl: home.concat(`post/${blogSlug.sixth}`),
    ogTitle: blogTitle.sixth,
    ogDesc: "Session storage snippets for login authentication in Preact-CLI and Django DRF.",
    twitterCard: "summary_large_image",
    twitterUrl: home.concat(`post/${blogSlug.sixth}`),
    twitterTitle: blogTitle.sixth,
    twitterDesc: "Session storage snippets for login authentication in Preact-CLI and Django DRF..",
    imgFB: "https://od.lk/s/ODhfOTEzOTIzMl8/storage.jpg",
    imgTwitter: "https://od.lk/s/ODhfOTEzOTIzMl8/storage.jpg",
    twitterCreator: creator,
    imgAlt: "Shipping container by Tom Fisk"
  },
];
